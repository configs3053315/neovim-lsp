vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)

-- When highlighted, J & K move the line up and down
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")


-- Navigate easily with tabs
vim.keymap.set("n", "tn", ":tabn<CR>")
vim.keymap.set("n", "tp", ":tabp<CR>")
vim.keymap.set("n", "tt", ":tabnew")

-- Preserved copy word to copy paste several times
vim.keymap.set("x", "<leader>p", "\"_dP")

-- auto search and replace word
vim.keymap.set("n", "<leader>s", ":%s/\\<<C-r><C-w>\\>/<C-r><C-w>/gI<Left><Left><Left>")
