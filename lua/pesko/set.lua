-- Relative Number
vim.opt.nu = true
vim.opt.relativenumber = true


-- Tab spaces
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true

vim.opt.smartindent = true


-- Setup undo dir to undo days ago
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

-- Highlight search
vim.opt.hlsearch = true

-- Colors
vim.opt.termguicolors = true
vim.opt.colorcolumn = "80"

-- The cursor will never going at the bottom of the file
-- only 8 minimum lines before the bottom
vim.opt.scrolloff = 8
vim.opt.signcolumn = "yes"
vim.opt.isfname:append("@-@")

-- Fast update time
vim.opt.updatetime = 50

-- Set map leader
vim.g.mapleader = " "
