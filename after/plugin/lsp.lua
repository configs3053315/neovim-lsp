-- Default configuration
local lsp = require('lsp-zero')

lsp.preset("recommended")

-- LSP Servers installed which I nned.
lsp.ensure_installed({
	"pylsp",
	"rust_analyzer",
})

-- Remap selection box
local cmp = require("cmp")
local cmp_select = {behavior = cmp.SelectBehavior.Select}
local cmp_mappings = lsp.defaults.cmp_mappings({
  ["<C-p>"] = cmp.mapping.select_prev_item(cmp_select),
  ["<C-n>"] = cmp.mapping.select_next_item(cmp_select),
  ["<C-y>"] = cmp.mapping.confirm({ select = true }),
  ["<C-Space>"] = cmp.mapping.complete(),
})

lsp.setup_nvim_cmp({
  mapping = cmp_mappings,
  preselect = "none",
  completion = {
    completeopt = "menu,menuone,noinsert,noselect"
  },
})


-- Remap keys section
-- The remap will attached on every new single buffer 
-- and will exist just only in this buffer

lsp.on_attach(function(client, bufnr)
  local opts = {buffer = bufnr, remap = false}

  vim.keymap.set("n", "gd", function() vim.lsp.buf.definition() end, opts)
  vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
  vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, opts)
  vim.keymap.set('n', '<leader>D', vim.lsp.buf.type_definition, opts)
end)

lsp.setup()
