function ColorMyPencils(color)
	-- set colors 
    require('onedark').setup {
        style = 'darker'
    }
    require('onedark').load()

	-- Set transparency
--	vim.api.nvim_set_hl(0, "Normal", { bg = "none" })
--	vim.api.nvim_set_hl(0, "NormalFloat", { bg = "none" })

end

ColorMyPencils()
