# Installation

Ripgrep
https://github.com/BurntSushi/ripgrep#installation

Packer nvim
https://github.com/wbthomason/packer.nvim

cd lua/pesko/packer.lua
:PackerSync

Colorscheme:
https://github.com/navarasu/onedark.nvim
